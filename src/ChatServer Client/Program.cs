﻿using System;
using System.Threading.Tasks;
using ChatServer.Common;
using nGrpc.Client;
using nGrpc.Client.GrpcServices;
using nGrpc.Common;
using System.Collections.Generic;

namespace ChatServer_Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.Write("Server address (default 127.0.0.1): ");
            string host = Console.ReadLine();
            host = string.IsNullOrWhiteSpace(host) == true ? "127.0.0.1" : host;

            Console.Write("Server port (default 5051): ");
            string portString = Console.ReadLine();
            portString = string.IsNullOrWhiteSpace(portString) == true ? "5051" : portString;
            int port = int.Parse(portString);


            GrpcChannel grpcChannel = new GrpcChannel();
            await grpcChannel.Connect(host, port);
            Console.WriteLine("Connected");

            ProfileGrpcService profileGrpcService = new ProfileGrpcService(grpcChannel);
            RegisterRes registerRes = await profileGrpcService.RegisterRPC(new RegisterReq());
            Console.WriteLine($"Registered. PlayerId:{registerRes.PlayerId}");

            LoginRes loginRes = await profileGrpcService.LoginRPC(new LoginReq { PlayerId = registerRes.PlayerId, SecretKey = registerRes.SecretKey });
            Console.WriteLine($"Logined. PlayerId:{loginRes.PlayerId}");

            ChatGrpcService chatGrpcService = new ChatGrpcService(grpcChannel);

            string input = null;
            while (true)
            {
                try
                {
                    Console.Write("Enter Command: ");
                    input = Console.ReadLine();
                    string[] inputParts = input.Split(' ');
                    string command = inputParts[0];

                    if (command == "exit")
                        break;
                    else if (command == "create")
                    {
                        CreateGroupRes createGroupRes = await chatGrpcService.CreateGroupRPC(new CreateGroupReq { GroupName = inputParts[1] });
                        if (createGroupRes.IsGroupCreated == true)
                            Console.WriteLine("Chat group is created.");
                        else
                            Console.WriteLine("There is a chat group with the same name.");
                    }
                    else if (command == "list")
                    {
                        GetAllGroupNamesRes getAllGroupNamesRes = await chatGrpcService.GetAllGroupNamesRPC(new GetAllGroupNamesReq());
                        Console.WriteLine($"All Groups Names: ");
                        foreach (KeyValuePair<string, int> pair in getAllGroupNamesRes.ChatGroupNames)
                            Console.WriteLine($"{pair.Key}: {pair.Value}");
                        Console.WriteLine();
                    }
                    else if (command == "join")
                    {
                        string groupName = inputParts[1];
                        JoinGroupRes joinGroupRes = await chatGrpcService.JoinGroupRPC(new JoinGroupReq { GroupName = groupName, PlayerName = inputParts[2] });
                        Console.Clear();
                        Console.WriteLine($"Joined group:\"{inputParts[1]}\" with name:\"{inputParts[2]}\"");

                        List<ChatMessage> messagesList;
                        int startMessageLine = 5;

                        void WriteMessagesList()
                        {
                            int cursorLeft = Console.CursorLeft;
                            int cursorTop = Console.CursorTop;

                            Console.SetCursorPosition(0, startMessageLine - 1);
                            Console.WriteLine();

                            for (int i = messagesList.Count - 1; i > messagesList.Count - 25 && i >= 0; i--)
                            {
                                Console.Write("\r" + new string(' ', Console.WindowWidth - 1) + "\r");
                                Console.WriteLine(ConvertChatMessageToString(messagesList[i]));
                            }

                            Console.SetCursorPosition(cursorLeft, cursorTop);
                        }

                        GetLatestMessagesRes getLatestMessagesRes = await chatGrpcService.GetLatestMessagesRPC(new GetLatestMessagesReq { GroupName = groupName });
                        messagesList = getLatestMessagesRes.LatestMessages;
                        WriteMessagesList();

                        async void OnChatReceived(ChatMessage chatMessage)
                        {
                            if (chatMessage.GroupName == groupName)
                            {
                                messagesList.Add(chatMessage);
                                await Task.Delay(100);
                                WriteMessagesList();
                            }
                        }
                        chatGrpcService.ChatReceived += OnChatReceived;

                        string text = "";
                        while (true)
                        {
                            Console.SetCursorPosition(0, 1);
                            Console.WriteLine();
                            Console.Write("\r" + new string(' ', Console.WindowWidth - 1) + "\r");
                            Console.Write("Write your message: ");
                            text = Console.ReadLine();
                            if (text == "!leave")
                                break;

                            SendMessageRes sendMessageRes = await chatGrpcService.SendMessageRPC(new SendMessageReq { GroupName = inputParts[1], Text = text });
                        }

                        chatGrpcService.ChatReceived -= OnChatReceived;
                        LeaveGroupRes leaveGroupRes = await chatGrpcService.LeaveGroupRPC(new LeaveGroupReq { GroupName = inputParts[1] });
                        Console.Clear();
                    }
                    else if (command == "clear")
                    {
                        Console.Clear();
                    }
                    else if (command == "help")
                    {
                        string help = @"
Commands List:
1- Exit: exit
2- Create new chat group: create [chat_group_name]
3- List existing chat groups: list
4- Join chat group: join [chat_group_name] [player_name]
5- Leave chat group: Write !leave when you are in chat goup
6- Clear screen: clear
";
                        Console.WriteLine(help);
                    }
                    else
                        Console.WriteLine("There is not such command!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        static string ConvertChatMessageToString(ChatMessage chatMessage)
        {
            return $"{chatMessage.PlayerName} ({chatMessage.Date}) : {chatMessage.Text}";
        }
    }
}

