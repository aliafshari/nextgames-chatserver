﻿using System;

namespace ChatServer.Common
{
    public class ChatMessage
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string GroupName { get; set; }
        public int PlayerId { get; set; }
        public string PlayerName { get; set; }
        public string Text { get; set; }
    }
}
