﻿using System.Collections.Generic;

namespace ChatServer.Common
{
    public class GetAllGroupNamesReq
    {

    }
    public class GetAllGroupNamesRes
    {
        public Dictionary<string, int> ChatGroupNames { get; set; }
    }

    public class CreateGroupReq
    {
        public string GroupName { get; set; }
    }
    public class CreateGroupRes
    {
        public bool IsGroupCreated { get; set; }
    }

    public class JoinGroupReq
    {
        public string GroupName { get; set; }
        public string PlayerName { get; set; }
    }
    public class JoinGroupRes
    {
        public bool IsPlayerJoined { get; set; }
    }

    public class LeaveGroupReq
    {
        public string GroupName { get; set; }
    }
    public class LeaveGroupRes
    {
        public bool IsPlayerLeaved { get; set; }
    }

    public class SendMessageReq
    {
        public string GroupName { get; set; }
        public string Text { get; set; }
    }
    public class SendMessageRes
    {

    }

    public class GetLatestMessagesReq
    {
        public string GroupName { get; set; }
    }
    public class GetLatestMessagesRes
    {
        public List<ChatMessage> LatestMessages { get; set; }
    }
}
