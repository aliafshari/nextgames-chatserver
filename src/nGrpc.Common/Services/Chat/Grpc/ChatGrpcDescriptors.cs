﻿using Grpc.Core;
using nGrpc.Common;

namespace ChatServer.Common
{
    public static class ChatGrpcDescriptors
    {
        public static readonly string ServiceName = "ChatService";

        public static Method<GetAllGroupNamesReq, GetAllGroupNamesRes> GetAllGroupNamesDesc =
            GrpcUtils.CreateMethodDescriptor<GetAllGroupNamesReq, GetAllGroupNamesRes>(MethodType.Unary, ServiceName, "GetAllGroupNames");

        public static Method<CreateGroupReq, CreateGroupRes> CreateGroupDesc =
           GrpcUtils.CreateMethodDescriptor<CreateGroupReq, CreateGroupRes>(MethodType.Unary, ServiceName, "CreateGroup");

        public static Method<JoinGroupReq, JoinGroupRes> JoinGroupDesc =
          GrpcUtils.CreateMethodDescriptor<JoinGroupReq, JoinGroupRes>(MethodType.Unary, ServiceName, "JoinGroup");

        public static Method<LeaveGroupReq, LeaveGroupRes> LeaveGroupDesc =
          GrpcUtils.CreateMethodDescriptor<LeaveGroupReq, LeaveGroupRes>(MethodType.Unary, ServiceName, "LeaveGroup");

        public static Method<SendMessageReq, SendMessageRes> SendMessageDesc =
         GrpcUtils.CreateMethodDescriptor<SendMessageReq, SendMessageRes>(MethodType.Unary, ServiceName, "SendMessage");

        public static Method<GetLatestMessagesReq, GetLatestMessagesRes> GetLatestMessagesDesc =
         GrpcUtils.CreateMethodDescriptor<GetLatestMessagesReq, GetLatestMessagesRes>(MethodType.Unary, ServiceName, "GetLatestMessages");
    }
}
