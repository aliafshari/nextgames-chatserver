﻿using ChatServer.Common;
using nGrpc.Client;
using nGrpc.Client.GrpcServices;
using nGrpc.Common;
using System.Threading.Tasks;
using Xunit;

namespace nGrpc.IntegrationTest
{
    public class ChatServiceTest : IntegrationTestBase
    {
        private async Task<ChatGrpcService> CreateUser_CreateGroup(string groupName)
        {
            (GrpcChannel grpcChannel, LoginRes loginRes) = await TestUtils.GetNewLoginedChannel();
            ChatGrpcService chatGrpcService = new ChatGrpcService(grpcChannel);

            CreateGroupReq req = new CreateGroupReq
            {
                GroupName = groupName
            };
            await chatGrpcService.CreateGroupRPC(req);
            return chatGrpcService;
        }


        [Fact]
        public async Task Chat_CreateGroupRPC_Test()
        {
            (GrpcChannel grpcChannel, LoginRes loginRes) = await TestUtils.GetNewLoginedChannel();
            ChatGrpcService chatGrpcService = new ChatGrpcService(grpcChannel);

            CreateGroupReq req = new CreateGroupReq
            {
                GroupName = "jklsdmcnbiuer"
            };
            CreateGroupRes res = await chatGrpcService.CreateGroupRPC(req);

            Assert.True(res.IsGroupCreated);
        }

        [Fact]
        public async Task Chat_GetAllGroupNamesRPC_Test()
        {
            string groupName = "fgasasd";
            var chatGrpcService = await CreateUser_CreateGroup(groupName);

            GetAllGroupNamesReq req = new GetAllGroupNamesReq();
            GetAllGroupNamesRes res = await chatGrpcService.GetAllGroupNamesRPC(req);

            Assert.Contains(groupName, res.ChatGroupNames.Keys);
        }

        [Fact]
        public async Task Chat_JoinGroupRPC_Test()
        {
            string groupName = "vbxcvb";
            var chatGrpcService = await CreateUser_CreateGroup(groupName);

            JoinGroupReq req = new JoinGroupReq
            {
                PlayerName = "Playername1",
                GroupName = groupName
            };
            JoinGroupRes res = await chatGrpcService.JoinGroupRPC(req);

            Assert.True(res.IsPlayerJoined);
        }

        [Fact]
        public async Task Chat_LeaveGroupRPC_Test()
        {
            string groupName = "mcnxcgh";
            var chatGrpcService = await CreateUser_CreateGroup(groupName);

            JoinGroupReq joinReq = new JoinGroupReq
            {
                PlayerName = "Playername1xcvc",
                GroupName = groupName
            };
            await chatGrpcService.JoinGroupRPC(joinReq);

            LeaveGroupReq req = new LeaveGroupReq
            {
                GroupName = groupName
            };
            LeaveGroupRes res = await chatGrpcService.LeaveGroupRPC(req);

            Assert.True(res.IsPlayerLeaved);
        }

        [Fact]
        public async Task Chat_SendMessageRPC_Test()
        {
            string groupName = "cvbcvbndfye";
            var chatGrpcService = await CreateUser_CreateGroup(groupName);

            JoinGroupReq joinReq = new JoinGroupReq
            {
                PlayerName = "cvxcvbmvbnghfghdye",
                GroupName = groupName
            };
            await chatGrpcService.JoinGroupRPC(joinReq);

            ChatMessage receivedChatMessage = null;
            chatGrpcService.ChatReceived += message => receivedChatMessage = message;

            SendMessageReq req = new SendMessageReq
            {
                GroupName = groupName,
                Text = "hello goupmates"
            };
            await chatGrpcService.SendMessageRPC(req);

            await TestUtils.WaitIfTrue(() => receivedChatMessage == null, 2000);

            Assert.Equal(joinReq.PlayerName, receivedChatMessage.PlayerName);
            Assert.Equal(req.Text, receivedChatMessage.Text);
        }

        [Fact]
        public async Task Chat_GetLatestMessagesRPC_Test()
        {
            string groupName = "xfgrfsdys";
            var chatGrpcService = await CreateUser_CreateGroup(groupName);

            JoinGroupReq joinReq = new JoinGroupReq
            {
                PlayerName = "vcbmo[uiop",
                GroupName = groupName
            };
            await chatGrpcService.JoinGroupRPC(joinReq);

            SendMessageReq sendReq = new SendMessageReq
            {
                GroupName = groupName,
                Text = "hello goupmates 567567345"
            };
            await chatGrpcService.SendMessageRPC(sendReq);

            GetLatestMessagesReq req = new GetLatestMessagesReq
            {
                GroupName = groupName
            };
            GetLatestMessagesRes res = await chatGrpcService.GetLatestMessagesRPC(req);

            Assert.Single(res.LatestMessages);
            Assert.Equal(joinReq.PlayerName, res.LatestMessages[0].PlayerName);
            Assert.Equal(sendReq.Text, res.LatestMessages[0].Text);
        }
    }
}
