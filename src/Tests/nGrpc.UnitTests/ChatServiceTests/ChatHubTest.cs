﻿using ChatServer.Common;
using ChatService;
using Microsoft.Extensions.Logging;
using nGrpc.Common;
using nGrpc.ServerCommon;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace nGrpc.UnitTests.ChatServiceTests
{
    public class ChatHubTest
    {
        IChatHub _chatHub;
        ChatConfigs _chatConfigs = new ChatConfigs
        {
            GroupCapacity = 3,
            GroupMessagesHistoryCount = 2,
            ChatSaveIntervalInMilisec = 10
        };

        ITime _time = Substitute.For<ITime>();
        IPubSubHub _pubSubHub = Substitute.For<IPubSubHub>();
        IChatRepository _chatRepository;

        public ChatHubTest()
        {
            _chatHub = new ChatHub(CreateChatGroupFactory(), _chatRepository);
        }

        IChatGroupFactory CreateChatGroupFactory()
        {
            _chatRepository = Substitute.For<IChatRepository>();
            _chatRepository.GetLatestChatMessages(Arg.Any<string>(), Arg.Any<int>()).Returns(x => new List<ChatMessage>());

            IChatGroupFactory chatGroupFactory = Substitute.For<IChatGroupFactory>();
            chatGroupFactory.CreateNewChatGroup(Arg.Any<string>()).Returns(x => CreateChatGroup(x.ArgAt<string>(0)));
            return chatGroupFactory;
        }

        ChatGroup CreateChatGroup(string groupName)
        {
            ILogger<ChatGroup> logger = Substitute.For<ILogger<ChatGroup>>();
            return new ChatGroup(logger, groupName, _chatConfigs, _time, _pubSubHub, _chatRepository);
        }


        [Fact]
        public async Task GIVEN_ChatHub_WHEN_Call_CreateGroup_With_A_New_GroupName_THEN_It_Should_Return_True()
        {
            // given
            int playerId = 8635;
            IChatHub chatHub = _chatHub;
            string groupName = "kjasdgfuiv";

            // when
            bool b = await chatHub.CreateGroup(playerId, groupName);

            // then
            Assert.True(b);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_WHEN_Call_CreateGroup_With_A_Repetitive_GroupName_THEN_It_Should_Return_False()
        {
            // given
            int playerId = 45673;
            IChatHub chatHub = _chatHub;
            string groupName = "kjasdgfuiv";
            await chatHub.CreateGroup(playerId, groupName);

            // when
            bool b = await chatHub.CreateGroup(playerId, groupName);

            // then
            Assert.False(b);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_Two_Created_Group_WHEN_Call_GetAllGroupNames_THEN_It_Should_Contain_Created_Groups()
        {
            // given
            IChatHub chatHub = _chatHub;

            int playerId1 = 45673;
            string groupName1 = "dfgxc";
            await chatHub.CreateGroup(playerId1, groupName1);

            int playerId2 = 455467653;
            string groupName2 = "dfggsfgsdfsvxxc";
            await chatHub.CreateGroup(playerId2 + 1, groupName2);

            // when
            Dictionary<string, int> names = chatHub.GetAllGroupNames();

            // then
            Assert.NotNull(names);
            Assert.Equal(2, names.Count);
            Assert.Contains(groupName1, names.Keys);
            Assert.Contains(groupName2, names.Keys);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_WHEN_Call_JoinGroup_And_Call_IsPlayerInGroup_THEN_It_Should_Return_True()
        {
            // given
            IChatHub chatHub = _chatHub;
            int playerId = 353;
            string playerName = "playerName35634";
            string groupName = "vxvcx";
            await chatHub.CreateGroup(playerId, groupName);

            // when
            await chatHub.JoinGroup(playerId, playerName, groupName);
            bool b = chatHub.IsPlayerInGroup(playerId, groupName);

            // then
            Assert.True(b);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_WHEN_Call_IsPlayerInGroup_THEN_It_Should_Return_False()
        {
            // given
            IChatHub chatHub = _chatHub;
            int playerId = 353;
            string groupName = "vxvcx";
            await chatHub.CreateGroup(playerId, groupName);

            // when
            bool b = chatHub.IsPlayerInGroup(playerId, groupName);

            // then
            Assert.False(b);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_WHEN_Call_JoinGroup_With_Wrong_GroupName_THEN_It_Should_Throw_ChatGroupDoesNotExistException()
        {
            // given
            IChatHub chatHub = _chatHub;
            int playerId = 456;
            string playerName = "playerName345242597874";
            string groupName = "vbxdffs";
            await chatHub.CreateGroup(playerId, groupName);

            // when
            Exception exception = await Record.ExceptionAsync(() => chatHub.JoinGroup(playerId, playerName, groupName + "asdf"));

            // then
            Assert.NotNull(exception);
            Assert.IsType<ChatGroupDoesNotExistException>(exception);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_Which_Is_Full_WHEN_Call_JoinGroup_THEN_It_Should_Return_False_And_Player_Should_Not_Be_In_The_Group()
        {
            // given
            IChatHub chatHub = _chatHub;
            int playerId = 34563;
            string playerName = "playerName";
            string groupName = "fghd";
            await chatHub.CreateGroup(playerId, groupName);
            for (int i = 1; i <= _chatConfigs.GroupCapacity; i++)
                await chatHub.JoinGroup(playerId + i, $"name_{i}", groupName);

            // when
            bool b = await chatHub.JoinGroup(playerId, playerName, groupName);
            bool isInGroup = chatHub.IsPlayerInGroup(playerId, groupName);

            // then
            Assert.False(b);
            Assert.False(isInGroup);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_And_Joined_Player_WHEN_Call_LeaveGroup_THEN_The_Player_Should_Not_Be_In_Group()
        {
            // given
            IChatHub chatHub = _chatHub;
            int playerId = 456;
            string playerName = "playerName345242597874";
            string groupName = "vbxdffs";
            await chatHub.CreateGroup(playerId, groupName);
            await chatHub.JoinGroup(playerId, playerName, groupName);

            // when
            bool b = await chatHub.LeaveGroup(playerId, groupName);
            bool b2 = await chatHub.LeaveGroup(playerId, groupName);

            // then
            Assert.True(b);
            Assert.False(b2);
            bool isInGroup = chatHub.IsPlayerInGroup(playerId, groupName);
            Assert.False(isInGroup);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_With_2_Joined_Players_WHEN_Call_SendMessage_THEN_The_Message_Should_Be_Sent_To_Both_Players()
        {
            // given
            IChatHub chatHub = _chatHub;
            _time.UTCTime.Returns(new DateTime(1, 2, 3));
            IPubSubHub pubSubHub = _pubSubHub;

            int playerId1 = 456;
            string playerName1 = "playerName345242597874";
            string groupName = "vbxdffs";
            await chatHub.CreateGroup(playerId1, groupName);
            await chatHub.JoinGroup(playerId1, playerName1, groupName);

            int playerId2 = 3533;
            string playerName2 = "asdfghgfhtyr";
            await chatHub.JoinGroup(playerId2, playerName2, groupName);

            string messageText = "hi there";

            // when
            chatHub.SendMessage(playerId1, groupName, messageText);

            // then
            ChatSentMessage chatSentMessage = new ChatSentMessage
            {
                ChatGroupPlayersIds = new List<int> { playerId1, playerId2 },
                ChatMessage = new ChatMessage
                {
                    Id = 1,
                    Date = _time.UTCTime,
                    GroupName = groupName,
                    PlayerId = playerId1,
                    PlayerName = playerName1,
                    Text = messageText,
                }
            };
            pubSubHub.Received(1).Publish(Arg.Is<ChatSentMessage>(message => message.ToJson() == chatSentMessage.ToJson()));
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_With_No_Joined_Player_WHEN_Call_SendMessage_THEN_It_Should_Throw_PlayerIsNotInChatGroupException()
        {
            // given
            IChatHub chatHub = _chatHub;
            int playerId = 45745;
            string groupName = "vbxdffs";
            await chatHub.CreateGroup(playerId, groupName);

            // when
            Exception exception = Record.Exception(() => chatHub.SendMessage(playerId, groupName, "chat text"));

            // then
            Assert.NotNull(exception);
            Assert.IsType<PlayerIsNotInChatGroupException>(exception);
        }

        [Fact]
        public async Task GIVEN_ChatHub_With_A_Created_Group_And_Some_ChatMessages_WHEN_Call_GetLatestMessages_THEN_It_Should_Return_Sent_Chat()
        {
            // given
            IChatHub chatHub = _chatHub;
            int playerId = 45745;
            string playerName = "vxbcvgsdtweruy";
            string groupName = "vbxdffs";
            string text = "chat text";
            await chatHub.CreateGroup(playerId, groupName);
            await chatHub.JoinGroup(playerId, playerName, groupName);
            for (int i = 0; i < _chatConfigs.GroupMessagesHistoryCount + 10; i++)
                chatHub.SendMessage(playerId, groupName, text);

            // when
            List<ChatMessage> chatMessages = chatHub.GetLatestMessages(playerId, groupName);

            // then
            Assert.NotNull(chatMessages);
            Assert.Equal(_chatConfigs.GroupMessagesHistoryCount, chatMessages.Count);
            var expectedMessage = new ChatMessage
            {
                Id = 11,
                Date = _time.UTCTime,
                GroupName = groupName,
                PlayerId = playerId,
                PlayerName = playerName,
                Text = text,
            };
            Assert.Equal(expectedMessage.ToJson(), chatMessages[0].ToJson());
        }
    }
}
