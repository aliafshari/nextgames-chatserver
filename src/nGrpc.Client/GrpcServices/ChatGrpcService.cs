﻿using ChatServer.Common;
using nGrpc.Common;
using System;
using System.Threading.Tasks;

namespace nGrpc.Client.GrpcServices
{
    public class ChatGrpcService
    {
        public event Action<ChatMessage> ChatReceived;

        private readonly GrpcChannel _grpcChannel;

        public ChatGrpcService(GrpcChannel grpcChannel)
        {
            _grpcChannel = grpcChannel;

            _grpcChannel.OnServerEventReceived += ServerEventReceivedCallback;
        }

        private void ServerEventReceivedCallback(ServerEventType serverEventType, string eventData)
        {
            if (serverEventType == ServerEventType.Chat)
            {
                ChatMessage chatMessage = eventData.ToObject<ChatMessage>();
                ChatReceived?.Invoke(chatMessage);
            }
        }

        public async Task<GetAllGroupNamesRes> GetAllGroupNamesRPC(GetAllGroupNamesReq req)
        {
            var res = await _grpcChannel.CallRpc(ChatGrpcDescriptors.GetAllGroupNamesDesc, req);
            return res;
        }

        public async Task<CreateGroupRes> CreateGroupRPC(CreateGroupReq req)
        {
            var res = await _grpcChannel.CallRpc(ChatGrpcDescriptors.CreateGroupDesc, req);
            return res;
        }

        public async Task<JoinGroupRes> JoinGroupRPC(JoinGroupReq req)
        {
            var res = await _grpcChannel.CallRpc(ChatGrpcDescriptors.JoinGroupDesc, req);
            return res;
        }

        public async Task<LeaveGroupRes> LeaveGroupRPC(LeaveGroupReq req)
        {
            var res = await _grpcChannel.CallRpc(ChatGrpcDescriptors.LeaveGroupDesc, req);
            return res;
        }

        public async Task<SendMessageRes> SendMessageRPC(SendMessageReq req)
        {
            var res = await _grpcChannel.CallRpc(ChatGrpcDescriptors.SendMessageDesc, req);
            return res;
        }

        public async Task<GetLatestMessagesRes> GetLatestMessagesRPC(GetLatestMessagesReq req)
        {
            var res = await _grpcChannel.CallRpc(ChatGrpcDescriptors.GetLatestMessagesDesc, req);
            return res;
        }
    }
}
