﻿using ChatServer.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatService
{
    public interface IChatRepository
    {
        Task SaveMessages(List<ChatMessage> chatMessages);
        Task<List<ChatMessage>> GetLatestChatMessages(string groupName, int lastChatsCount);
        Task<List<string>> GetAllGroupNames();
        Task SaveGroupName(int playerId, string groupName);
        Task UpdateGroupJoinedPlayers(string groupName, Dictionary<int, string> playerIdToName);
        Task<Dictionary<int, string>> GetGroupJoinedPlayers(string groupName);
    }
}