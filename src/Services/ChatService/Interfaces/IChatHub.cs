﻿using ChatServer.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatService
{
    public interface IChatHub
    {
        Task<bool> CreateGroup(int playerId, string groupName);
        Dictionary<string, int> GetAllGroupNames();
        Task<bool> JoinGroup(int playerId, string playerName, string groupName);
        bool IsPlayerInGroup(int playerId, string groupName);
        Task<bool> LeaveGroup(int playerId, string groupName);
        void SendMessage(int playerId, string groupName, string messageText);
        List<ChatMessage> GetLatestMessages(int playerId, string groupName);
    }
}
