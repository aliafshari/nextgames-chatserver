﻿namespace ChatService
{
    public interface IChatGroupFactory
    {
        ChatGroup CreateNewChatGroup(string groupName);
    }
}
