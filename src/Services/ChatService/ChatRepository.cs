﻿using ChatServer.Common;
using Dapper;
using nGrpc.Common;
using nGrpc.ServerCommon;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatService
{
    public class ChatRepository : IChatRepository
    {
        private readonly IDBProvider _dBProvider;

        public ChatRepository(IDBProvider dBProvider)
        {
            _dBProvider = dBProvider;
        }


        public async Task<List<string>> GetAllGroupNames()
        {
            using (var conn = _dBProvider.GetConnection())
            {
                await conn.OpenAsync();
                string query = "select name from chatGroups;";
                IEnumerable<string> names = await conn.QueryAsync<string>(query);
                return names?.ToList();
            }
        }

        public async Task SaveGroupName(int playerId, string groupName)
        {
            using (var conn = _dBProvider.GetConnection())
            {
                await conn.OpenAsync();
                string query = "insert into ChatGroups(name, creatorId) values(@GroupName, @CreatorId);";
                await conn.ExecuteAsync(query, new { GroupName = groupName, CreatorId = playerId });
            }
        }


        public async Task<List<ChatMessage>> GetLatestChatMessages(string groupName, int lastChatsCount)
        {
            using (var conn = _dBProvider.GetConnection())
            {
                await conn.OpenAsync();
                string query = @"
select array_to_json(array_agg(data)) from
(select cm.data from 
chatMessages cm join ChatGroups cg on cm.GroupId = cg.Id
where cg.Name = @GroupName
order by cm.id desc
limit @Count) as a;";
                string json = await conn.QueryFirstOrDefaultAsync<string>(query, new { GroupName = groupName, Count = lastChatsCount });

                var res = json.ToObject<List<ChatMessage>>() ?? new List<ChatMessage>();
                return res.OrderBy(n => n.Id).ToList();
            }
        }

        public async Task SaveMessages(List<ChatMessage> chatMessages)
        {
            if (chatMessages == null || chatMessages.Count == 0)
                return;

            using (var conn = _dBProvider.GetConnection())
            {
                await conn.OpenAsync();
                string query = "insert into chatMessages(id, groupId, data) values(@Id, (select id from chatGroups where name=@GroupName), @Data::jsonb);";
                var parametersList = chatMessages.Select(n => new { Id = n.Id, GroupName = n.GroupName, Data = n.ToJson() });
                await conn.ExecuteAsync(query, parametersList);
            }
        }

        public async Task UpdateGroupJoinedPlayers(string groupName, Dictionary<int, string> playerIdToName)
        {
            using (var conn = _dBProvider.GetConnection())
            {
                await conn.OpenAsync();
                string query = "update chatGroups set joinedPlayers = @JoinedPlayers::jsonb where name = @GroupName;";
                await conn.ExecuteAsync(query, new { GroupName = groupName, JoinedPlayers = playerIdToName.ToJson() });
            }
        }

        public async Task<Dictionary<int, string>> GetGroupJoinedPlayers(string groupName)
        {
            using (var conn = _dBProvider.GetConnection())
            {
                await conn.OpenAsync();
                string query = "select joinedPlayers from chatGroups where name = @GroupName;";
                string json = await conn.QuerySingleOrDefaultAsync<string>(query, new { GroupName = groupName });
                return json.ToObject<Dictionary<int, string>>();
            }
        }
    }
}
