﻿using Microsoft.Extensions.Logging;
using nGrpc.ServerCommon;

namespace ChatService
{
    public class ChatGroupFactory : IChatGroupFactory
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly ITime _time;
        private readonly ChatConfigs _chatConfigs;
        private readonly IChatRepository _chatRepository;
        private readonly IPubSubHub _pubsub;

        public ChatGroupFactory(
            ILoggerFactory loggerFactory,
            ITime time,
            ChatConfigs chatConfigs,
            IChatRepository chatRepository,
            IPubSubHub pubsub)
        {
            _loggerFactory = loggerFactory;
            _time = time;
            _chatConfigs = chatConfigs;
            _chatRepository = chatRepository;
            _pubsub = pubsub;
        }

        public ChatGroup CreateNewChatGroup(string groupName)
        {
            ChatGroup chatGroup = new ChatGroup(_loggerFactory.CreateLogger<ChatGroup>(), groupName, _chatConfigs, _time, _pubsub, _chatRepository);
            return chatGroup;
        }
    }
}
