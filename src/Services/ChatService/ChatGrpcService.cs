﻿using ChatServer.Common;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using nGrpc.Common;
using nGrpc.ServerCommon;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatService
{
    public class ChatGrpcService : IGrpcService
    {
        private readonly ILogger<ChatGrpcService> _logger;
        private readonly IChatHub _chatHub;
        private readonly IPubSubHub _pubSubHub;
        private readonly IServerEventStreamsManager _serverEventStreamsManager;

        public ChatGrpcService(ILogger<ChatGrpcService> logger,
            IChatHub chatHub,
            IPubSubHub pubSubHub,
            IServerEventStreamsManager serverEventStreamsManager)
        {
            _logger = logger;
            _chatHub = chatHub;
            _pubSubHub = pubSubHub;
            _serverEventStreamsManager = serverEventStreamsManager;

            _pubSubHub.Subscribe<ChatSentMessage>(this, async message => await ChatSentMessageHandler(message));
        }

        private async Task ChatSentMessageHandler(ChatSentMessage chatSentMessage)
        {
            try
            {
                ServerEventRes serverEventRes = new ServerEventRes
                {
                    ServerEventType = ServerEventType.Chat,
                    CustomData = chatSentMessage.ChatMessage.ToJson()
                };
                await _serverEventStreamsManager.SendEventToPlayers(chatSentMessage.ChatGroupPlayersIds, serverEventRes);

                _logger.LogInformation("ChatSentMessageHandler, PlayersIds:{pis}, CustomData:{cd}", chatSentMessage.ChatGroupPlayersIds, serverEventRes.CustomData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }

        public void AddRpcMethods(IGrpcBuilderAdapter grpcBuilder)
        {
            grpcBuilder.AddMethod(ChatGrpcDescriptors.GetAllGroupNamesDesc, GetAllGroupNamesRPC);
            grpcBuilder.AddMethod(ChatGrpcDescriptors.CreateGroupDesc, CreateGroupRPC);
            grpcBuilder.AddMethod(ChatGrpcDescriptors.JoinGroupDesc, JoinGroupRPC);
            grpcBuilder.AddMethod(ChatGrpcDescriptors.LeaveGroupDesc, LeaveGroupRPC);
            grpcBuilder.AddMethod(ChatGrpcDescriptors.SendMessageDesc, SendMessageRPC);
            grpcBuilder.AddMethod(ChatGrpcDescriptors.GetLatestMessagesDesc, GetLatestMessagesRPC);
        }


        public async Task<GetAllGroupNamesRes> GetAllGroupNamesRPC(GetAllGroupNamesReq req, ServerCallContext context)
        {
            int playerId = context.GetPlayerCredential().PlayerId;
            Dictionary<string, int> names = _chatHub.GetAllGroupNames();

            _logger.LogInformation("GetAllGroupNamesRPC, PlayerId:{pi}, GroupNames:{gn}", playerId, names.ToJson());

            return new GetAllGroupNamesRes
            {
                ChatGroupNames = names
            };
        }

        public async Task<CreateGroupRes> CreateGroupRPC(CreateGroupReq req, ServerCallContext context)
        {
            int playerId = context.GetPlayerCredential().PlayerId;
            bool b = await _chatHub.CreateGroup(playerId, req.GroupName);

            _logger.LogInformation("CreateGroupRPC, PlayerId:{pi}, GroupName:{gn}, IsGroupCreated:{igc}", playerId, req.GroupName, b);

            return new CreateGroupRes
            {
                IsGroupCreated = b
            };
        }

        public async Task<JoinGroupRes> JoinGroupRPC(JoinGroupReq req, ServerCallContext context)
        {
            int playerId = context.GetPlayerCredential().PlayerId;
            bool b = await _chatHub.JoinGroup(playerId, req.PlayerName, req.GroupName);

            _logger.LogInformation("JoinGroupRPC, PlayerId:{pi}, GroupName:{gn}, IsPlayerJoined:{ipj}", playerId, req.GroupName, b);

            return new JoinGroupRes
            {
                IsPlayerJoined = b
            };
        }

        public async Task<LeaveGroupRes> LeaveGroupRPC(LeaveGroupReq req, ServerCallContext context)
        {
            int playerId = context.GetPlayerCredential().PlayerId;
            bool b = await _chatHub.LeaveGroup(playerId, req.GroupName);

            _logger.LogInformation("LeaveGroupRPC, PlayerId:{pi}, GroupName:{gn}, IsPlayerLeaved:{ipj}", playerId, req.GroupName, b);

            return new LeaveGroupRes
            {
                IsPlayerLeaved = b
            };
        }

        public async Task<SendMessageRes> SendMessageRPC(SendMessageReq req, ServerCallContext context)
        {
            int playerId = context.GetPlayerCredential().PlayerId;
            _chatHub.SendMessage(playerId, req.GroupName, req.Text);

            _logger.LogInformation("SendMessageRPC, PlayerId:{pi}, GroupName:{gn}, Text:{text}", playerId, req.GroupName, req.Text);

            return new SendMessageRes
            {

            };
        }

        public async Task<GetLatestMessagesRes> GetLatestMessagesRPC(GetLatestMessagesReq req, ServerCallContext context)
        {
            int playerId = context.GetPlayerCredential().PlayerId;
            List<ChatMessage> messages = _chatHub.GetLatestMessages(playerId, req.GroupName);

            _logger.LogInformation("GetLatestMessagesRPC, PlayerId:{pi}, GroupName:{gn}", playerId, req.GroupName);

            return new GetLatestMessagesRes
            {
                LatestMessages = messages
            };
        }
    }
}
