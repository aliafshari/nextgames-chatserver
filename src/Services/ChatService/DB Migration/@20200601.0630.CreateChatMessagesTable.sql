﻿create table ChatMessages(
	id int,
	groupId int not null,
	data jsonb not null,
	primary key (id, groupId)
);