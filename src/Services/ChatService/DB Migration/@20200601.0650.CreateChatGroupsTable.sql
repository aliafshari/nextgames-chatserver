﻿create table ChatGroups(
	id serial4 primary key,
	name text not null unique,
	creatorId int not null,
	joinedPlayers jsonb default '{}'
);