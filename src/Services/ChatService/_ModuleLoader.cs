﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using nGrpc.ServerCommon;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatService
{
    public class ModuleLoader : IModuleLoader
    {
        public void AddServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IChatRepository, ChatRepository>();
            services.AddSingleton<IChatHub, ChatHub>();
            services.AddSingleton<IChatGroupFactory, ChatGroupFactory>();
            services.AddConfig<ChatConfigs>(configuration, "ChatConfigs");
            services.AddSingleton<IGrpcService, ChatGrpcService>();
        }

        public List<(int priorityIndex, Func<Task> func)> Initializer(IServiceProvider serviceProvider)
        {
            return null;
        }
    }
}
