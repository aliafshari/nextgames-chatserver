﻿using ChatServer.Common;
using Microsoft.Extensions.Logging;
using nGrpc.ServerCommon;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ChatService
{
    public class ChatGroup
    {
        public string Name { get; private set; }
        public int JoinedPlayersCount => _playerIdToName.Count;

        private readonly AsyncLock _asyncLock = new AsyncLock();
        private Dictionary<int, string> _playerIdToName = new Dictionary<int, string>();

        private readonly ILogger<ChatGroup> _logger;
        private readonly ChatConfigs _chatConfigs;
        private readonly ITime _time;
        private readonly IPubSubHub _pubSubHub;
        private readonly IChatRepository _chatRepository;

        private int _lastChatId = 0;
        private int _lastSavedChatId = 0;
        private List<ChatMessage> _chatMessages;

        public ChatGroup(ILogger<ChatGroup> logger,
            string groupName,
            ChatConfigs chatConfigs,
            ITime time,
            IPubSubHub pubSubHub,
            IChatRepository chatRepository)
        {
            _logger = logger;
            Name = groupName;
            _chatConfigs = chatConfigs;
            _time = time;
            _pubSubHub = pubSubHub;
            _chatRepository = chatRepository;

            InitializeChatGroup().GetAwaiter().GetResult();
            Scheduler().NoAwait();
        }


        // private methods

        private async Task InitializeChatGroup()
        {
            _playerIdToName = await _chatRepository.GetGroupJoinedPlayers(Name) ?? new Dictionary<int, string>();

            _chatMessages = await _chatRepository.GetLatestChatMessages(Name, _chatConfigs.GroupMessagesHistoryCount);
            if (_chatMessages.Count > 0)
            {
                int lastChatMessageId = _chatMessages.Last().Id;
                _lastChatId = lastChatMessageId;
                _lastSavedChatId = lastChatMessageId;
            }
        }

        private async Task Scheduler()
        {
            try
            {
                while (true)
                {
                    await Task.Delay(_chatConfigs.ChatSaveIntervalInMilisec);

                    await SaveNewChatMessagesToDB();
                    RemoveRedundantCachedChatMessages();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }

        private void RemoveRedundantCachedChatMessages()
        {
            using (_asyncLock.Lock())
            {
                if (_chatMessages.Count > _chatConfigs.GroupMessagesHistoryCount)
                {
                    int redundantChatsCount = _chatMessages.Count - _chatConfigs.GroupMessagesHistoryCount;
                    _chatMessages.RemoveRange(0, redundantChatsCount);
                }
            }
        }

        private async Task SaveNewChatMessagesToDB()
        {
            List<ChatMessage> shouldSaveChats;
            using (await _asyncLock.LockAsync())
                shouldSaveChats = _chatMessages.Where(n => n.Id > _lastSavedChatId).ToList();

            if (shouldSaveChats.Count > 0)
            {
                await _chatRepository.SaveMessages(shouldSaveChats);
                _lastSavedChatId = shouldSaveChats.Last().Id;
            }
        }

        private void CheckIfPlayerIsInGroup(int playerId)
        {
            if (_playerIdToName.ContainsKey(playerId) == false)
                throw new PlayerIsNotInChatGroupException($"PlayerId:{playerId}, GroupName:{Name}");
        }


        // public methods

        public async Task<bool> Join(int playerId, string playerName)
        {
            using (_asyncLock.Lock())
            {
                if (_playerIdToName.Count >= _chatConfigs.GroupCapacity)
                    return false;

                _playerIdToName[playerId] = playerName;
                await _chatRepository.UpdateGroupJoinedPlayers(Name, _playerIdToName);

                return true;
            }
        }

        public bool IsPlayerJoined(int playerId)
        {
            return _playerIdToName.ContainsKey(playerId);
        }

        public async Task<bool> Leave(int playerId)
        {
            using (_asyncLock.Lock())
            {
                bool b = _playerIdToName.Remove(playerId);
                await _chatRepository.UpdateGroupJoinedPlayers(Name, _playerIdToName);
                return b;
            }
        }

        public void SendMessage(int playerId, string messageText)
        {
            CheckIfPlayerIsInGroup(playerId);

            ChatMessage chatMessage = new ChatMessage
            {
                Id = Interlocked.Increment(ref _lastChatId),
                Date = _time.UTCTime,
                GroupName = Name,
                PlayerId = playerId,
                PlayerName = _playerIdToName[playerId],
                Text = messageText
            };

            using (_asyncLock.Lock())
                _chatMessages.Add(chatMessage);

            ChatSentMessage chatSentMessage = new ChatSentMessage
            {
                ChatGroupPlayersIds = _playerIdToName.Keys.ToList().CloneByMessagePack(),
                ChatMessage = chatMessage
            };
            _pubSubHub.Publish(chatSentMessage);
        }

        public List<ChatMessage> GetLatestMessages(int playerId)
        {
            CheckIfPlayerIsInGroup(playerId);

            using (_asyncLock.Lock())
                return _chatMessages.TakeLast(_chatConfigs.GroupMessagesHistoryCount).ToList();
        }
    }
}
