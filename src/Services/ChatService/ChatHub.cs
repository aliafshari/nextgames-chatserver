﻿using ChatServer.Common;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatService
{
    public class ChatHub : IChatHub
    {
        private readonly IChatGroupFactory _chatGroupFactory;
        private readonly IChatRepository _chatRepository;
        private readonly ConcurrentDictionary<string, ChatGroup> _groupNameToGroup = new ConcurrentDictionary<string, ChatGroup>();

        public ChatHub(IChatGroupFactory chatGroupFactory,
            IChatRepository chatRepository)
        {
            _chatGroupFactory = chatGroupFactory;
            _chatRepository = chatRepository;

            InitializeChatHub().GetAwaiter().GetResult();
        }

        // private methods

        private async Task InitializeChatHub()
        {
            List<string> groupNames = await _chatRepository.GetAllGroupNames();
            if (groupNames != null)
                foreach (string groupName in groupNames)
                    CreateNewChatGroup(groupName);
        }

        private ChatGroup GetChatGroup(int playerId, string groupName)
        {
            if (_groupNameToGroup.TryGetValue(groupName, out ChatGroup chatGroup) == false)
                throw new ChatGroupDoesNotExistException($"PlayerId:{playerId}, GroupName:{groupName}");
            return chatGroup;
        }

        private bool CreateNewChatGroup(string groupName)
        {
            ChatGroup newChatGroup = _chatGroupFactory.CreateNewChatGroup(groupName);
            bool b = _groupNameToGroup.TryAdd(groupName, newChatGroup);
            return b;
        }


        // public methods

        public async Task<bool> CreateGroup(int playerId, string groupName)
        {
            bool b = CreateNewChatGroup(groupName);

            if (b == true)
                await _chatRepository.SaveGroupName(playerId, groupName);

            return b;
        }

        public Dictionary<string, int> GetAllGroupNames()
        {
            Dictionary<string, int> names = _groupNameToGroup.Values.ToDictionary(n => n.Name, m => m.JoinedPlayersCount);
            return names;
        }

        public async Task<bool> JoinGroup(int playerId, string playerName, string groupName)
        {
            ChatGroup chatGroup = GetChatGroup(playerId, groupName);
            bool b = await chatGroup.Join(playerId, playerName);
            return b;
        }

        public bool IsPlayerInGroup(int playerId, string groupName)
        {
            ChatGroup chatGroup = GetChatGroup(playerId, groupName);
            return chatGroup.IsPlayerJoined(playerId);
        }

        public async Task<bool> LeaveGroup(int playerId, string groupName)
        {
            ChatGroup chatGroup = GetChatGroup(playerId, groupName);
            return await chatGroup.Leave(playerId);
        }

        public void SendMessage(int playerId, string groupName, string messageText)
        {
            ChatGroup chatGroup = GetChatGroup(playerId, groupName);
            chatGroup.SendMessage(playerId, messageText);
        }

        public List<ChatMessage> GetLatestMessages(int playerId, string groupName)
        {
            ChatGroup chatGroup = GetChatGroup(playerId, groupName);
            return chatGroup.GetLatestMessages(playerId);
        }
    }
}
