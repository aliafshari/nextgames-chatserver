﻿using System;

namespace ChatService
{
    public class ChatGroupDoesNotExistException : Exception
    {
        public ChatGroupDoesNotExistException()
        {
        }

        public ChatGroupDoesNotExistException(string message) : base(message)
        {
        }
    }

    public class PlayerIsNotInChatGroupException : Exception
    {
        public PlayerIsNotInChatGroupException()
        {
        }

        public PlayerIsNotInChatGroupException(string message) : base(message)
        {
        }
    }
}
