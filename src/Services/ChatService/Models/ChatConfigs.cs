﻿namespace ChatService
{
    public class ChatConfigs
    {
        public int GroupCapacity { get; set; }
        public int GroupMessagesHistoryCount { get; set; }
        public int ChatSaveIntervalInMilisec { get; set; }
    }
}
