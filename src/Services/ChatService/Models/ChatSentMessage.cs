﻿using ChatServer.Common;
using System.Collections.Generic;

namespace ChatService
{
    public class ChatSentMessage
    {
        public List<int> ChatGroupPlayersIds { get; set; }
        public ChatMessage ChatMessage { get; set; }
    }
}
